---
title: About us
subtitle: More info about the LUG
comments: false
---

The Lawrence Linux User Group was started in 2014 as a place for Linux users to meet, learn, and network.  All skill levels from beginner to expert are invited to learn and share.  

What you can expect:

- Meet like-minded individuals
- Discuss Linux topics
- Get help with any issues
- Share knowledge and lessons learned

Some of the past topics that have been presented are:

- Intro to Linux
- Git
- Ansible 
- SSH
- Mysql
- Alternative phone operating systems
- Mycroft and language processing
- Various desktop environments and utilities
