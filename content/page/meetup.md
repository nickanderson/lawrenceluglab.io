---
title: Meetup info
subtitle: Come join us!
comments: false
---

Time: 3pm the third Saturday of each month

Place: Lawrence Public Library room A

Add to calendar: [LINK](https://calendar.google.com/calendar/ical/kansaslug%40gmail.com/public/basic.ics)

We meet on the third Saturday each month at the Lawrence Public Library.  We're going to try meeting room A going forward so we can give a remote option.  All experience levels are welcome to ask questions, get help, present a topic, request a topic to be presented, etc.  Please reach out via the IRC chat if there are any questions.

Lawrence Public Library info:
https://lplks.org/visit/
